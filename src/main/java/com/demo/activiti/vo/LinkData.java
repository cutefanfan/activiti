package com.demo.activiti.vo;

import java.io.Serializable;

import lombok.Data;

@Data
public class LinkData implements Serializable {

	private String id;

	private String name;

	private String from;

	private String to;

}
