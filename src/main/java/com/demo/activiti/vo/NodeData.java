package com.demo.activiti.vo;

import java.io.Serializable;

import lombok.Data;

@Data
public class NodeData implements Serializable {

	private String id;

	private String code;

	private String name;

	private Integer type;

	private Integer approveType;

	private String taskId;

	private String taskName;

	private String assignee;

}
