package com.demo.activiti.vo;

import lombok.Data;

@Data
public class NodeVo {

	private String nodeName;

	private String nodeAssigne;

	private String nodeStatu;

}
