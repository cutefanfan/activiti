package com.demo.activiti.vo;

import lombok.Data;

@Data
public class TaskData {

	private String id;

	private String name;

	private String assignee;

}
