package com.demo.activiti.enums;

/**
 * <p>项目名称: activiti </p> 
 * <p>文件名称: LeaveType.java </p> 
 * <p>类型描述: [枚举] </p>
 * <p>创建时间: 2019年9月25日 </p>
 * @author lifanfan
 * @version V1.0
 * @update 2019年9月25日 上午9:50:00 lifanfan 
 */
public enum LeaveType {
	SUBMIT_LEACE("1001", "已提交"), TL_APPROVE("1002", "主管审批"), HR_APPROVE("1003", "人事审批"), ERROR_LEAVE(
			"1004", "已取消"
	), END_LEAVE("1005", "已完成");

	private String code;
	private String description;

	LeaveType(String code, String description) {
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
