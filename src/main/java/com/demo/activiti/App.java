package com.demo.activiti;

import org.activiti.spring.boot.SecurityAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

/**
 * 启动类
 * 
 */
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class,
		org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class})
@ComponentScans({@ComponentScan("com.demo.activiti")})
public class App {

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}
}
