package com.demo.activiti.dto;

import java.io.Serializable;
import java.util.List;

import com.demo.activiti.vo.LinkData;
import com.demo.activiti.vo.NodeData;

import lombok.Data;

@Data
public class CreateProcess implements Serializable {

	private String id;

	private String code;

	private String name;

	private String formId;

	private List<LinkData> links;

	private List<NodeData> nodes;
}
