package com.demo.activiti.config;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * <p>项目名称: common-base </p> 
 * <p>文件名称: SrpingBeanUtil.java </p> 
 * <p>类型描述: [从spring容器中获取bean工具] </p>
 * <p>创建时间: 2019年9月3日 </p>
 * @author fuweiheng
 * @version V1.0
 * @update 2019年9月5日 下午5:42:10 lifanfan 
 */
@Component
public class SpringBeanUtil implements ApplicationContextAware {

	private static ApplicationContext applicationContext;

	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		SpringBeanUtil.applicationContext = applicationContext;
	}

	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	/**
	 * <p>功能描述: [通过bean名称从spring容器中获取bean对象] </p>
	 * @Title getBean
	 * @param <T> .
	 * @param type .
	 * @throws BeansException
	 * @CreateDate 2019年9月24日 下午2:37:03
	 * @update 2019年9月24日 下午2:37:03  lifanfan    
	 */
	public static <T> T getBean(Class<T> type) throws BeansException {
		return applicationContext.getBean(type);
	}
}
