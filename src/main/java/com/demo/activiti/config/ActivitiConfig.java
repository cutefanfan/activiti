package com.demo.activiti.config;

import java.io.IOException;

import javax.sql.DataSource;

import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.ManagementService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.impl.history.HistoryLevel;
import org.activiti.spring.SpringProcessEngineConfiguration;
import org.activiti.spring.boot.ProcessEngineConfigurationConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

@Configuration
// 使用activiti-spring-boot-starter-basic依赖无需此配置
public class ActivitiConfig implements ProcessEngineConfigurationConfigurer {

	/**
	 * 解決工作流生成图片乱码问题
	 *
	 * @param processEngineConfiguration processEngineConfiguration
	 */
	@Override
	public void configure(SpringProcessEngineConfiguration processEngineConfiguration) {
		processEngineConfiguration.setActivityFontName("宋体");
		processEngineConfiguration.setAnnotationFontName("宋体");
		processEngineConfiguration.setLabelFontName("宋体");
	}

	@Bean
	public ProcessEngine processEngine(DataSourceTransactionManager transactionManager, DataSource dataSource)
			throws IOException {
		SpringProcessEngineConfiguration configuration = new SpringProcessEngineConfiguration();

		// 自动部署已有的流程文件
		Resource[] resources = new PathMatchingResourcePatternResolver()
				.getResources(ResourceLoader.CLASSPATH_URL_PREFIX + "processes/*");
		configuration.setTransactionManager(transactionManager);
		configuration.setDataSource(dataSource);
		// 自动创建表
		configuration.setDatabaseSchemaUpdate("true");
		// 自动部署流程图
		configuration.setDeploymentResources(resources);
		// 使用activiti的身份信息
		configuration.setDbIdentityUsed(false);
		// 记录所有的流程信息
		configuration.setHistoryLevel(HistoryLevel.FULL);
		configuration.setMailServerDefaultFrom("1587534157@qq.com");
		configuration.setMailServerHost("smtp.qq.com");
		configuration.setMailServerPort(25);
		configuration.setMailServerPassword("snzpvhhfiwyahadi");
		configuration.setMailServerUsername("1587534157@qq.com");
		configuration.setMailServerUseSSL(true);
		return configuration.buildProcessEngine();
	}

	@Bean
	public RepositoryService repositoryService(ProcessEngine processEngine) {
		return processEngine.getRepositoryService();
	}

	@Bean
	public RuntimeService runtimeService(ProcessEngine processEngine) {
		return processEngine.getRuntimeService();
	}

	@Bean
	public TaskService taskService(ProcessEngine processEngine) {
		return processEngine.getTaskService();// 未完成的
	}

	@Bean
	public HistoryService historyService(ProcessEngine processEngine) {
		return processEngine.getHistoryService();// 历史任务
	}

	@Bean
	public ManagementService managementService(ProcessEngine processEngine) {
		return processEngine.getManagementService();
	}

	@Bean
	public IdentityService identityService(ProcessEngine processEngine) {
		return processEngine.getIdentityService();
	}

}
