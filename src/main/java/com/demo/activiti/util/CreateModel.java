package com.demo.activiti.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.activiti.bpmn.BpmnAutoLayout;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.EndEvent;
import org.activiti.bpmn.model.ExclusiveGateway;
import org.activiti.bpmn.model.Process;
import org.activiti.bpmn.model.SequenceFlow;
import org.activiti.bpmn.model.StartEvent;
import org.activiti.bpmn.model.UserTask;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import com.demo.activiti.config.SpringBeanUtil;
import com.demo.activiti.dto.CreateProcess;

import lombok.extern.slf4j.Slf4j;

/**
 * <p>项目名称: activiti </p> 
 * <p>文件名称: CreateModel.java </p> 
 * <p>类型描述: [自定义BpmnModel] </p>
 * <p>创建时间: 2019年11月22日 </p>
 * @author lifanfan
 * @version V1.0
 * @update 2019年11月22日 上午10:55:19 lifanfan 
 */
@Slf4j
public class CreateModel {

	// 获取核心工厂类
	private static ProcessEngine processEngine = SpringBeanUtil.getBean(ProcessEngine.class);
	//
	public void createTest() throws IOException {
		CreateProcess temp = new CreateProcess();
		temp.setCode("my-process");
		temp.setName("测试流程");
		// 创建bpmn模型
		BpmnModel model = new BpmnModel();
		org.activiti.bpmn.model.Process process = new Process();
		model.addProcess(process);
		process.setId(temp.getCode());
		process.setName(temp.getName());
		// process.setId("my-process");
		// process.setName("测试流程");
		// Process process = createProcess("my-process");
		// 创建bpmn元素
		// List<NodeData> nodes = new ArrayList<NodeData>();
		// NodeData node1 = new NodeData();
		// node1.setType(0);
		// NodeData node2 = new NodeData();
		// node2.setType(2);
		// node2.setTaskId("task1");
		// node2.setTaskName("task1");
		// node2.setAssignee("测试一");
		// NodeData node3 = new NodeData();
		// node3.setType(2);
		// node3.setTaskId("task2");
		// node3.setTaskName("task2");
		// node3.setAssignee("测试二");
		// NodeData node4 = new NodeData();
		// node4.setType(1);
		// nodes.add(node1);
		// nodes.add(node2);
		// nodes.add(node3);
		// nodes.add(node4);
		// for (NodeData nodeData : nodes) {
		// if (nodeData.getType() == 0) {
		// process.addFlowElement(createStartEvent());
		// } else if (nodeData.getType() == 2) {
		// process.addFlowElement(
		// createUserTask(nodeData.getTaskId(), nodeData.getTaskName(),
		// nodeData.getAssignee()));
		// } else if (nodeData.getType() == 1) {
		// process.addFlowElement(createEndEvent());
		// }
		// }
		// process.addFlowElement(createStartEvent());
		// process.addFlowElement(createUserTask("task2", "Second task", "john"));
		// process.addFlowElement(createUserTask("task1", "为啥开始结束不显示名称First task",
		// "fred"));

		// process.addFlowElement(createEndEvent());
		// List<LinkData> links = new ArrayList<LinkData>();
		// LinkData link1 = new LinkData();
		// link1.setFrom("start");
		// link1.setTo("task1");
		// LinkData link2 = new LinkData();
		// link2.setFrom("task1");
		// link2.setTo("task2");
		// LinkData link3 = new LinkData();
		// link3.setFrom("task2");
		// link3.setTo("end");
		// links.add(link1);
		// links.add(link2);
		// links.add(link3);
		// for (LinkData linkData : links) {
		// process.addFlowElement(createSequenceFlow(linkData.getFrom(),
		// linkData.getTo()));
		// }

		// process.addFlowElement(createSequenceFlow("start", "task1"));
		// process.addFlowElement(createSequenceFlow("task1", "task2"));
		// process.addFlowElement(createSequenceFlow("task2", "end"));

		process.addFlowElement(createStartEvent());
		process.addFlowElement(createUserTaskGroups("task1", "节点01", "candidateGroup1"));
		process.addFlowElement(createExclusiveGateway("createExclusiveGateway1"));
		process.addFlowElement(createUserTaskGroups("task2", "节点02", "candidateGroup2"));
		process.addFlowElement(createExclusiveGateway("createExclusiveGateway2"));
		process.addFlowElement(createUserTaskGroups("task3", "节点03", "candidateGroup3"));
		process.addFlowElement(createExclusiveGateway("createExclusiveGateway3"));
		process.addFlowElement(createUserTaskGroups("task4", "节点04", "candidateGroup4"));
		process.addFlowElement(createEndEvent());

		process.addFlowElement(createSequenceFlow("start", "task1", "", ""));
		process.addFlowElement(createSequenceFlow("task1", "task2", "", ""));
		process.addFlowElement(createSequenceFlow("task2", "createExclusiveGateway1", "", ""));
		process.addFlowElement(createSequenceFlow("createExclusiveGateway1", "task1", "不通过", "${pass=='2'}"));
		process.addFlowElement(createSequenceFlow("createExclusiveGateway1", "task3", "通过", "${pass=='1'}"));
		process.addFlowElement(createSequenceFlow("task3", "createExclusiveGateway2", "", ""));
		process.addFlowElement(createSequenceFlow("createExclusiveGateway2", "task2", "不通过", "${pass=='2'}"));
		process.addFlowElement(createSequenceFlow("createExclusiveGateway2", "task4", "通过", "${pass=='1'}"));
		process.addFlowElement(createSequenceFlow("task4", "createExclusiveGateway3", "", ""));
		process.addFlowElement(createSequenceFlow("createExclusiveGateway3", "task3", "不通过", "${pass=='2'}"));
		process.addFlowElement(createSequenceFlow("createExclusiveGateway3", "end", "通过", "${pass=='1'}"));

		// 2.生成BPMN自动布局
		new BpmnAutoLayout(model).execute();

		// 3.部署流程图
		Deployment deployment = processEngine.getRepositoryService().createDeployment()
				.addBpmnModel("my-process.bpmn", model).name("my-processbpmn").deploy();
		// 4.启动流程
		ProcessInstance processInstance = processEngine.getRuntimeService().startProcessInstanceByKey("my-process");
		// System.err.println("部署id" + processInstance.getId());
		// System.err.println("部署名称" + processInstance.getName());

		// 6.存储流程 图片
		InputStream inputsream = processEngine.getRepositoryService()
				.getProcessDiagram(processInstance.getProcessDefinitionId());
		// InputStream inputsream = FlowUtil.getProcessImage(processInstance.getId(),
		// "png");
		FileUtils.copyInputStreamToFile(inputsream, new File("E:/createTest_1.png"));
		// 7. 存储xml
		InputStream processBpmn = processEngine.getRepositoryService().getResourceAsStream(deployment.getId(),
				"my-process.bpmn");
		FileUtils.copyInputStreamToFile(processBpmn, new File("E:/createTest_1.bpmn20.xml"));
		System.err.println("执行完毕");
	}

	// 创建bpmn模型
	private Process createProcess(String processId) {
		BpmnModel model = new BpmnModel();
		Process process = new Process();
		model.addProcess(process);
		process.setId(processId);
		return process;
	}

	/**
	 * <p>功能描述: [创建任务节点,单人审批] </p>
	 * @Title createUserTask
	 * @param id
	 * @param name
	 * @param assignee
	 * @return
	 * @CreateDate 2019年11月25日 上午11:23:11
	 * @author lifanfan
	 * @update 2019年11月25日 上午11:23:11      
	 */
	protected UserTask createUserTask(String id, String name, String assignee) {
		UserTask userTask = new UserTask();
		userTask.setName(name);
		userTask.setId(id);
		userTask.setAssignee(assignee);
		return userTask;
	}

	/* 任务节点 */
	protected static UserTask createUserTaskGroups(String id, String name, String candidateGroup) {
		List<String> candidateGroups = new ArrayList<String>();
		candidateGroups.add(candidateGroup);
		UserTask userTask = new UserTask();
		userTask.setName(name);
		userTask.setId(id);
		userTask.setCandidateGroups(candidateGroups);
		return userTask;
	}

	/**
	 * 创建任务节点
	 * 多人审批
	 */
	public static UserTask createUsersTask(String id, String name, List<String> assignee) {
		UserTask userTask = new UserTask();
		userTask.setName(name);
		userTask.setId(id);
		userTask.setCandidateUsers(assignee);
		return userTask;
	}

	// 创建箭头
	protected SequenceFlow createSequenceFlow(String from, String to) {
		SequenceFlow flow = new SequenceFlow();
		flow.setSourceRef(from);
		flow.setTargetRef(to);
		return flow;
	}

	public static SequenceFlow createSequenceFlow(String from, String to, String name, String condition) {
		SequenceFlow flow = new SequenceFlow();
		flow.setSourceRef(from);
		flow.setTargetRef(to);
		flow.setName(name);

		if (!StringUtils.isEmpty(condition)) {
			flow.setConditionExpression(condition);
		}
		return flow;
	}

	/* 排他网关 */
	protected static ExclusiveGateway createExclusiveGateway(String id) {
		ExclusiveGateway exclusiveGateway = new ExclusiveGateway();
		exclusiveGateway.setId(id);
		return exclusiveGateway;
	}

	// 创建开始事件
	protected StartEvent createStartEvent() {
		StartEvent startEvent = new StartEvent();
		startEvent.setId("start");
		startEvent.setName("测试开始事件");
		return startEvent;
	}

	// 创建结束事件
	protected EndEvent createEndEvent() {
		EndEvent endEvent = new EndEvent();
		endEvent.setId("end");
		return endEvent;
	}

	// 根据节点类型判断事件类型,开始事件为0,结束事件为1,其他类型为2

	// Activities根据类型approveType判断走何种流程,开始事件为-2,结束事件为-1
}
