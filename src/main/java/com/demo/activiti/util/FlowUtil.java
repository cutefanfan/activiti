package com.demo.activiti.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.ExclusiveGateway;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.bpmn.model.FlowNode;
import org.activiti.bpmn.model.SequenceFlow;
import org.activiti.bpmn.model.StartEvent;
import org.activiti.bpmn.model.UserTask;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricVariableInstance;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.task.Task;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import com.alibaba.druid.util.Base64;
import com.demo.activiti.config.SpringBeanUtil;
import com.demo.activiti.vo.NodeVo;

import de.odysseus.el.ExpressionFactoryImpl;
import de.odysseus.el.util.SimpleContext;
import lombok.extern.slf4j.Slf4j;

/**
 * 流程工具类
 * @author fuweiheng
 * @date 2019年9月18日
 * @Description:
 */
@Slf4j
public class FlowUtil {

	private static ProcessEngine processEngine = SpringBeanUtil.getBean(ProcessEngine.class);

	/**
	 * 获取流程的流程图
	 * @param processDefId
	 * @return
	 */
	public static InputStream getProcessImage(String instanceId, String imgType) {
		HistoricProcessInstance historicProcessInstance = processEngine.getHistoryService()
				.createHistoricProcessInstanceQuery().processInstanceId(instanceId).singleResult();
		BpmnModel bpmnModel = processEngine.getRepositoryService()
				.getBpmnModel(historicProcessInstance.getProcessDefinitionId());
		InputStream img = processEngine.getProcessEngineConfiguration().getProcessDiagramGenerator()
				.generateDiagram(bpmnModel, imgType, getHighLight(historicProcessInstance, bpmnModel));
		return img;
	}

	/**
	 * 获取流程图所有节点信息
	 * @param instanceId
	 * @return
	 */
	public static List<NodeVo> getAllTask(String instanceId) {

		Task task = processEngine.getTaskService().createTaskQuery().processInstanceId(instanceId).singleResult();
		HistoricProcessInstance historicProcessInstance = processEngine.getHistoryService()
				.createHistoricProcessInstanceQuery().processInstanceId(instanceId).singleResult();
		BpmnModel model = processEngine.getRepositoryService()
				.getBpmnModel(historicProcessInstance.getProcessDefinitionId());
		// 查询正在运行的节点
		UserTask run = null;
		if (task != null) {
			ExecutionEntity executionEntity = (ExecutionEntity) processEngine.getRuntimeService().createExecutionQuery()
					.executionId(task.getExecutionId()).singleResult();
			String activitid = executionEntity.getActivityId();
			run = (UserTask) model.getMainProcess().getFlowElement(activitid);
		}
		List<HistoricVariableInstance> variableInstancesList = processEngine.getHistoryService()
				.createHistoricVariableInstanceQuery().processInstanceId(instanceId).list();
		// 查出开始节点
		FlowElement start = model.getMainProcess().getFlowElement(historicProcessInstance.getStartActivityId());
		// 查询出流程所有task
		List<UserTask> list = getNextElement(model, start.getId(), new ArrayList<UserTask>(), variableInstancesList);

		List<NodeVo> nodeList = new ArrayList<NodeVo>();
		List<HistoricActivityInstance> activitiList = processEngine.getHistoryService()
				.createHistoricActivityInstanceQuery().processInstanceId(historicProcessInstance.getId()).finished()
				.orderByHistoricActivityInstanceEndTime().asc().list();
		for (UserTask userTask : list) {
			boolean temp = false;
			for (HistoricActivityInstance historicActivityInstance : activitiList) {
				FlowElement f = model.getMainProcess().getFlowElement(historicActivityInstance.getActivityId());
				if (f instanceof UserTask) {
					if (userTask.getId().equals(historicActivityInstance.getActivityId())) {
						NodeVo nodeVo = new NodeVo();
						nodeVo.setNodeName(userTask.getName());
						nodeVo.setNodeAssigne(historicActivityInstance.getAssignee());
						nodeVo.setNodeStatu("end");
						nodeList.add(nodeVo);
						temp = true;
						break;
					}
				}
			}
			if (temp == false) {
				NodeVo nodeVo = new NodeVo();
				nodeVo.setNodeName(userTask.getName());
				nodeVo.setNodeAssigne(userTask.getAssignee());

				if (run != null && run.getId().equals(userTask.getId())) {
					nodeVo.setNodeStatu("run");
				} else {
					nodeVo.setNodeStatu("ready");
				}
				nodeList.add(nodeVo);

			}

		}
		return nodeList;
	}

	/**
	 * 查询下一个节点
	 * @param model
	 * @param elementId
	 * @return
	 */
	public static List<UserTask> getNextElement(BpmnModel model, String elementId, List<UserTask> next,
			List<HistoricVariableInstance> variableInstancesList) {

		// 得到当前节点
		FlowNode flowElement = (FlowNode) model.getMainProcess().getFlowElement(elementId);

		if (flowElement instanceof ExclusiveGateway) {// 当前节点排他路由
			// 根据路由条件来选择节点
			ExpressionFactory factory = new ExpressionFactoryImpl();
			SimpleContext context = new SimpleContext();
			for (HistoricVariableInstance historicVariableInstance : variableInstancesList) {
				String key = historicVariableInstance.getVariableName();
				Object value = historicVariableInstance.getValue();
				context.setVariable(key, factory.createValueExpression(value, String.class));
			}
			// 得到流出线
			List<SequenceFlow> flow = flowElement.getOutgoingFlows();
			for (SequenceFlow sequenceFlow : flow) {
				String ex = sequenceFlow.getConditionExpression();
				// 校验表达式是否成立
				ValueExpression e = factory.createValueExpression(context, ex, boolean.class);
				boolean result = (boolean) e.getValue(context);
				if (result) {
					FlowElement nextNode = sequenceFlow.getTargetFlowElement();
					getNextElement(model, nextNode.getId(), next, variableInstancesList);
					break;
				}

			}
		} else if (flowElement instanceof UserTask) {
			// 得到流出线
			List<SequenceFlow> flow = flowElement.getOutgoingFlows();
			for (SequenceFlow sequenceFlow : flow) {
				FlowElement nextNode = sequenceFlow.getTargetFlowElement();
				String assigne = ((UserTask) flowElement).getAssignee();
				// 如果使用的是表达式 将其设置为具体的处理人
				if (!StringUtils.isEmpty(assigne) && assigne.startsWith("$")) {
					String as = assigne.substring(assigne.lastIndexOf("{") + 1, assigne.lastIndexOf("}"));
					for (HistoricVariableInstance historicVariableInstance : variableInstancesList) {
						String key = historicVariableInstance.getVariableName();
						String value = historicVariableInstance.getValue().toString();
						if (as.equals(key)) {
							((UserTask) flowElement).setAssignee(value);
							break;
						}
					}

				}
				next.add((UserTask) flowElement);
				getNextElement(model, nextNode.getId(), next, variableInstancesList);
			}

		} else if (flowElement instanceof StartEvent) {
			// next.add(flowElement);
			// 得到流出线
			List<SequenceFlow> flow = flowElement.getOutgoingFlows();
			for (SequenceFlow sequenceFlow : flow) {
				FlowElement nextNode = sequenceFlow.getTargetFlowElement();
				// next.add(nextNode);
				getNextElement(model, nextNode.getId(), next, variableInstancesList);
			}
		}
		return next;
	}

	/**
	 * 获取高亮显示节点
	 * @param historicProcessInstance
	 * @param bpmnModel
	 * @return
	 */
	public static List<String> getHighLight(HistoricProcessInstance historicProcessInstance, BpmnModel bpmnModel) {
		// 获取流程中已经执行的节点并且先后排序
		List<HistoricActivityInstance> activitiList = processEngine.getHistoryService()
				.createHistoricActivityInstanceQuery().processInstanceId(historicProcessInstance.getId()).finished()
				.orderByHistoricActivityInstanceId().asc().list();
		// 高亮展示
		List<String> highLight = new ArrayList<String>();
		for (HistoricActivityInstance historicActivityInstance : activitiList) {
			highLight.add(historicActivityInstance.getActivityId());// 添加活动节点
			// 添加节点的连线
			FlowNode flowNode = (FlowNode) bpmnModel.getMainProcess()
					.getFlowElement(historicActivityInstance.getActivityId());// 得到节点信息
			List<SequenceFlow> pvmTransitions = flowNode.getIncomingFlows(); // 取出所有的到该节点的线
			if (!CollectionUtils.isEmpty(pvmTransitions)) {
				for (SequenceFlow sequenceFlow : pvmTransitions) {
					highLight.add(sequenceFlow.getId());
				}
			}
		}

		return highLight;
	}

	/**
	 * 将stream转换成base64
	 * @param inputStream
	 * @return
	 * @throws IOException
	 */
	public static String streamToBase64(InputStream inputStream, String type) {
		String pre = "data:image/" + type + ";base64,";
		byte[] bytes = null;
		try {
			bytes = IOUtils.toByteArray(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
			log.error("stream 转换成 base64 错误");
		}
		String base64 = pre + Base64.byteArrayToBase64(bytes);
		return base64;
	}
}
